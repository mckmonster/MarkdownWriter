[![pipeline status](https://gitlab.com/mckmonster/MarkdownWriter/badges/master/pipeline.svg)](https://gitlab.com/mckmonster/MarkdownWriter/commits/master)

# MarkdownWriter
Class to write de [Markdown File](https://fr.wikipedia.org/wiki/Markdown)

## How to use it

```csharp
var markdown = new MarkdownWriter("c:\tmp\file.md");
markdown.WriteTitle("My title");
markdown.WriteLine("my text");
markdown.WriteBlockCode("void function()\n{\n}");
```

### The result should be

># My Title
>my text
>```
>void function()
>{
>}
>```
