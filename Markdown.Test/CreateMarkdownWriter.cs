﻿using Markdown;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnit.Markdown
{
    [TestFixture]
    public class MarkdownWriterTest
    {
        [Test]
        public void Create_WithPath()
        {
            var writer = new MarkdownWriter(Path.GetTempFileName());
        }

        [Test]
        public void Create_MemoryStream()
        {
            var memory = new MemoryStream();
            var writer = new MarkdownWriter(memory);
        }

        [Test]
        public void Write_Line()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.WriteLine("Test line");

            var result = writer.ToString();
            StringAssert.AreEqualIgnoringCase("Test line\r\n", result);
        }

        [Test]
        public void Write_Line_Bold()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.WriteLine("Test line", MarkdownWriter.Emphasis.Bold);

            var result = writer.ToString();
            StringAssert.AreEqualIgnoringCase("**Test line**\r\n", result);
        }

        [Test]
        public void Write_Line_Italic()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.WriteLine("Test line", MarkdownWriter.Emphasis.Italic);

            var result = writer.ToString();
            StringAssert.AreEqualIgnoringCase("_Test line_\r\n", result);
        }

        [Test]
        public void Write_Line_BoldItalic()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.WriteLine("Test line", MarkdownWriter.Emphasis.BoldItalic);

            var result = writer.ToString();
            StringAssert.AreEqualIgnoringCase("_**Test line**_\r\n", result);
        }

        [Test]
        public void Write()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.Write("Test");
            writer.Write("Element");

            var result = writer.ToString();
            StringAssert.AreEqualIgnoringCase("Test Element", result);
        }

        [Test]
        public void Write_Bold()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.Write("Test");
            writer.Write("Element", MarkdownWriter.Emphasis.Bold);

            var result = writer.ToString();
            StringAssert.AreEqualIgnoringCase("Test **Element**", result);
        }

        [Test]
        public void Write_Italic()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.Write("Test");
            writer.Write("Element", MarkdownWriter.Emphasis.Italic);

            var result = writer.ToString();
            StringAssert.AreEqualIgnoringCase("Test _Element_", result);
        }

        [Test]
        public void Write_BoldItalic()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.Write("Test");
            writer.Write("Element", MarkdownWriter.Emphasis.BoldItalic);

            var result = writer.ToString();
            StringAssert.AreEqualIgnoringCase("Test _**Element**_", result);
        }

        [Test]
        public void Write_Title_Zero()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            Assert.Throws<ArgumentException>(() =>
            {
                writer.WriteTitle("Test title", 0);
            });
        }

        [Test]
        public void Write_Title([Range(1, 5)]int level)
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.WriteTitle("Test title", (uint)level);

            var lvlstring = "#";
            for (int i = 1; i < level; ++i)
            {
                lvlstring += "#";
            }
            var result = writer.ToString();
            StringAssert.StartsWith($"{lvlstring} Test title", result);
        }

        [Test]
        public void Write_SmartList_Zero()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            Assert.Throws<ArgumentException>(() =>
            {
                writer.WriteSmartList("Test element", 0);
            });
        }

        [Test]
        public void Write_SmartList([Range(1, 5)]int level)
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.WriteSmartList("Test element", (uint)level);

            var lvlstring = string.Empty;
            for (int i = 1; i < level; ++i)
            {
                lvlstring += "\t";
            }
            lvlstring += "*";

            var result = writer.ToString();
            StringAssert.StartsWith($"{lvlstring} Test element", result);
        }

        [Test]
        public void Write_NumberedList()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.WriteNumberedList("Test element");
            writer.WriteNumberedList("Test element");
            writer.WriteNumberedList("Test element");
            writer.WriteNumberedList("Test element", true);

            var result = writer.ToString();
            var lines = result.Split('\n');
            var lines2 = lines.Where(line => !string.IsNullOrEmpty(line));
            for (int i = 0; i < lines2.Count() - 1; ++i)
            {    
                StringAssert.StartsWith($"{i+1}. Test element", lines2.ElementAt(i));
            }

            StringAssert.StartsWith($"1. Test element", lines2.ElementAt(lines2.Count() - 1));
        }

        [Test]
        public void Write_Citation()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.WriteCitation("Test Citation");

            var result = writer.ToString();
            StringAssert.StartsWith(">Test Citation", result);
        }

        [Test]
        public void Write_Citation_MultiLine()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            writer.WriteCitation("Test Citation\nTest Citation 2");

            var result = writer.ToString();
            StringAssert.StartsWith(">Test Citation\r\n>Test Citation 2", result);
        }

        [Test]
        public void Write_CodeBlock()
        {
            var writer = new MarkdownWriter(new MemoryStream());

            var code = new StringBuilder();

            code.AppendLine("public void toto()");
            code.AppendLine("{");
            code.AppendLine("}");

            writer.WriteBlocCode(code.ToString());

            var result = writer.ToString();
            StringAssert.StartsWith("\tpublic void toto()\r\n\t{\r\n\t}", result);
        }

        [Test]
        public void Write_Code()
        {
            var writer = new MarkdownWriter(new MemoryStream());
            writer.Write("Test");
            writer.WriteCode("function test()");

            var result = writer.ToString();
            StringAssert.AreEqualIgnoringCase("Test `function test()`", result);
        }

        [Test]
        public void Write_Link()
        {
            var writer = new MarkdownWriter(new MemoryStream());
            writer.WriteLink("Link", "http://");

            var result = writer.ToString();
            StringAssert.AreEqualIgnoringCase("[Link](http://)", result);
        }

        [Test]
        public void Write_ImageLink()
        {
            var writer = new MarkdownWriter(new MemoryStream());
            writer.WriteLink("Link", "http://", true);

            var result = writer.ToString();
            StringAssert.AreEqualIgnoringCase("![Link](http://)", result);
        }

        [Test]
        public void AddSeparator()
        {
            var writer = new MarkdownWriter(new MemoryStream());
            writer.AddSeparator();

            var result = writer.ToString();
            StringAssert.AreEqualIgnoringCase("\r\n\r\n-----------------\r\n", result);
        }

        [Test]
        public void CheckAll()
        {
            var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "markdown_test.md");
            using (var reader = File.OpenText(path))
            {
                using (var writer = new MarkdownWriter(new MemoryStream()))
                {
                    writer.WriteTitle("Test Markdown");
                    writer.WriteTitle("Paragraph", 2);

                    writer.Write("Test Paragraph with");
                    writer.Write("bold", MarkdownWriter.Emphasis.Bold);
                    writer.Write(",");
                    writer.Write("italic", MarkdownWriter.Emphasis.Italic);
                    writer.Write("and");
                    writer.Write("bold italic", MarkdownWriter.Emphasis.BoldItalic);
                    writer.WriteLine("And");
                    writer.Write("Test de code :");
                    writer.WriteCode("function()");

                    writer.WriteTitle("Code", 2);
                    writer.WriteBlocCode("void function()");
                    writer.WriteBlocCode("{");
                    writer.WriteBlocCode("\t//Comment");
                    writer.WriteBlocCode("}");

                    writer.WriteTitle("Citation", 2);
                    writer.WriteCitation("Test de citation");
                    writer.WriteCitation("une vrai citation");

                    writer.WriteTitle("List", 2);
                    writer.WriteSmartList("puce 1");
                    writer.WriteSmartList("puce 2");
                    writer.WriteSmartList("puce 3");
                    writer.WriteNumberedList("list 1");
                    writer.WriteNumberedList("list 2");
                    writer.WriteNumberedList("list 3");
                    StringAssert.AreEqualIgnoringCase(reader.ReadToEnd(), writer.ToString());
                }
            }
        }
    }
}
