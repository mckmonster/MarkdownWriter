﻿using System;
using System.IO;
using System.Text;

namespace Markdown
{
    public class MarkdownWriter : IDisposable
    {
        private readonly StreamWriter _writer;
        private StringBuilder builder = new StringBuilder();
    
        public enum Emphasis
        {
            None,
            Bold,
            Italic,
            BoldItalic
        }

        /// <summary>
        /// Create a MarkdownWriter in file on specific path
        /// </summary>
        /// <param name="path">Path of the file</param>
        public MarkdownWriter(string path)
        {
            _writer = File.CreateText(path);
            _writer.AutoFlush = true;
        }

        /// <summary>
        /// Create a MarkdownWriter in a stream
        /// </summary>
        /// <param name="stream">Stream where we write markdown</param>
        public MarkdownWriter(Stream stream)
        {
            _writer = new StreamWriter(stream)
            {
                AutoFlush = true
            };
        }

        /// <summary>
        /// Write a simple text
        /// </summary>
        /// <param name="text">text to write</param>
        /// <param name="emphasis">style of text</param>
        public void Write(string text, Emphasis emphasis = Emphasis.None)
        {
            switch (emphasis)
            {
                case Emphasis.Bold:
                    text = $"**{text}**";
                    break;
                case Emphasis.Italic:
                    text = $"_{text}_";
                    break;
                case Emphasis.BoldItalic:
                    text = $"_**{text}**_";
                    break;
            }
            if (HaveText())
            {
                text = $" {text}";
            }

            builder.Append(text);
            _writer.Write(text);
        }

        public void WriteLine()
        {
            builder.AppendLine();
            _writer.WriteLine();
            builder.AppendLine();
            _writer.WriteLine();
        }

        /// <summary>
        /// Write a paragraph
        /// </summary>
        /// <param name="text">Text to write</param>
        /// <param name="emphasis">style of text</param>
        public void WriteLine(string text, Emphasis emphasis = Emphasis.None)
        {
            if (HaveText())
            {
                builder.AppendLine();
                _writer.WriteLine();
            }

            Write($"{text}", emphasis);

            builder.AppendLine();
            _writer.WriteLine();
        }

        /// <summary>
        /// Write a markdown title
        /// </summary>
        /// <param name="title">Title to write</param>
        /// <param name="level">Title's level should be higher than 0</param>
        public void WriteTitle(string title, uint level = 1)
        {
            if (level == 0)
                throw new ArgumentException("Should be higher than 0", nameof(level));

            var lvlstring = "#";
            for (int i = 1; i < level; ++i)
            {
                lvlstring += "#";
            }
            WriteLine($"{lvlstring} {title}");
        }

        /// <summary>
        /// Write a Markdown smart list
        /// </summary>
        /// <param name="element">Element of list</param>
        /// <param name="level">Level of element in list, should be higher than 0</param>
        public void WriteSmartList(string element, uint level = 1)
        {
            if (level == 0)
                throw new ArgumentException("Should be higher than 0", nameof(level));

            var lvlstring = string.Empty;
            for (int i = 1; i < level; ++i)
            {
                lvlstring += "\t";
            }
            lvlstring += "*";

            WriteLine($"{lvlstring} {element}");
        }

        private int _numbered = 1;
        /// <summary>
        /// Write a numbered list
        /// </summary>
        /// <param name="element">Element of list</param>
        /// <param name="reset">If you want to force return to 1st number</param>
        public void WriteNumberedList(string element, bool reset = false)
        {
            if (reset)
                _numbered = 1;

            WriteLine($"{_numbered}. {element}");
            _numbered++;
        }

        /// <summary>
        /// Write Citation
        /// </summary>
        /// <param name="citation">Citation to write</param>
        public void WriteCitation(string citation)
        {
            var citations = citation.Split('\n');
            foreach (var cit in citations)
            {
                WriteLine($">{cit}");
            }
        }

        /// <summary>
        /// Write a block code
        /// </summary>
        /// <param name="code">code to write</param>
        public void WriteBlocCode(string code)
        {
            var codes = code.Split('\n');
            foreach (var cod in codes)
            {
                WriteLine($"\t{cod.TrimEnd('\r')}");
            }
        }


        /// <summary>
        /// Write code part
        /// </summary>
        /// <param name="code">code to write</param>
        public void WriteCode(string code)
        {
            Write($"`{code}`");
        }

        /// <summary>
        /// Write a link
        /// </summary>
        /// <param name="text">text to write</param>
        /// <param name="link">link on clic</param>
        /// <param name="image">true if it's image link</param>
        public void WriteLink(string text, string link, bool image = false)
        {
            if (image)
            {
                Write($"![{text}]({link})");
            }
            else
            {
                Write($"[{text}]({link})");
            }
        }

        /// <summary>
        /// Add a separator
        /// </summary>        
        public void AddSeparator()
        {
            WriteLine();
            WriteLine("-----------------");
        }

        /// <summary>
        /// Flush text on stream
        /// </summary>
        public void Flush()
        {
            _writer.Flush();
        }

        public override string ToString()
        {
            return builder.ToString();
        }

        public void Dispose()
        {
            _writer.Dispose();
        }

        private bool HaveText()
        {
            return builder.Length != 0 && !builder.ToString().EndsWith("\r\n");
        }
    }
}
